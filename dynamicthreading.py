from threading import Thread, Event

class SmartThread(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *,daemon=None):
        super().__init__(group=group,target=target,name=name,args=args,kwargs=kwargs,daemon=daemon)
        self._terminate = Event()
        self._resume = Event()
        self._auto_suspend = True
        self._debug = False
        self.suspended = Event()

    def _dbg(self,string):
        if(self._debug == False):
            return
        assert type(string) is str
        print(self._name+str(" "+string))

    def setTarget(self,target=None,args=(),kwargs=None):
        assert type(args) is list or type(args) is tuple
        self._target = target
        self._args = args
        if(kwargs == None):
            kwargs = {}
        self._kwargs = kwargs

    def run(self):
        try:
            if self._target:
                while not self._terminate.is_set():
                    self._resume.clear()
                    self._target(*self._args,**self._kwargs)
                    if(self._auto_suspend):
                        while not self._resume.is_set():
                            self._dbg("SUSPENDED")
                            self.suspended.set()
                            self._resume.wait()
                            self.suspended.clear()
                            self._dbg(("RESUMED"))
                    if(self._terminate.is_set()):
                        self._dbg("Terminating")
                        break
        finally:
            del self._target, self._args, self._kwargs

    def resume(self):
        self._resume.set()

    def suspend(self):
        self._resume.clear()

    def terminate(self):
        self.resume()
        self._terminate.set()

    def is_finished(self):
        b = self.suspended.is_set()# and not self._resume.is_set()
        return b

    def setDebug(self,boolean):
        self._debug = boolean

    def block(self):
        #blocks the calling thread until the function has finished processing
        if(self.suspended.is_set()):
            return True
        else:
            self.suspended.wait()

class ThreadManager(object):
    def __init__(self):
        self._container = {}
        self._thread_count = 0
        self._daemon_default = True
        self._THREADS_TERMINATED = False

    def setupThreads(self, number):
        while (self._thread_count != number):    
            self._thread_count += 1
            thr = SmartThread(group=None,target=None,name=str(self._thread_count),args=())
            thr.daemon = self._daemon_default
            self._container[self._thread_count] = thr

    def newThread(self,target=None,args=(),name=None):
        assert type(args) is list or type(args) is tuple
        self._thread_count += 1
        if not name:
            name = str(self._thread_count)
        thr = SmartThread(group=None,target=target,name=name,args=args)
        thr.daemon = self._daemon_default
        self._container[self._thread_count] = thr

    def startAll(self):
        self._check_container()
        if self._THREADS_TERMINATED:
            raise RuntimeError("Terminated threads cannot be started again")

        for ID,thr in self._container.items():
            thr.start()

    def resumeAll(self):
        self._check_container()
        for ID,thr in self._container.items():
            if(thr.is_alive()):
                thr.resume()

    def terminateAll(self):
        self._check_container()
        for ID,thr in self._container.items():
            thr.resume()
            thr.terminate()
            thr.join()
        self._THREADS_TERMINATED = True
        self.freeAll()

    def freeAll(self):
        self._THREADS_TERMINATED = False
        self._container = {}
        self._thread_count = 0

    def _check_container(self):
        if(self._container == {}):
            raise ValueError("Cannot perform operation on empty thread list")

    def setDaemonic(boolean):
        self._daemon_default = boolean

    def setTarget(self,thread_number,function,args):
        assert type(args) is list or type(args) is tuple
        self._container[thread_number].setTarget(function,args)


