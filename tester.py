from dynamicthreading import ThreadManager
from time import sleep
def testrun(var):
    print("Running with "+str(var))

def somefunc(var):
    print("Another function with "+str(var))
    sleep(8)
    print("Done")

print("-------------Setting up thread manager and threads")
tm = ThreadManager()
tm.setupThreads(5)
tm.setTarget(1,testrun,["some variable"])
tm.setTarget(2,testrun,["another variable"])
tm.setTarget(3,testrun,["variable"])
tm.setTarget(4,testrun,["variable X"])
tm.setTarget(5,testrun,["more nonsense"])
print("-------------Starting all threads")
tm.startAll()
sleep(5)
print("-------------Resuming with same function")
tm.resumeAll()
sleep(5)
print("-------------Reconfiguring the function to call by reusing threads")
tm.setTarget(1,somefunc,["some variable"])
tm.setTarget(2,somefunc,["another variable"])
tm.setTarget(3,somefunc,["variable"])
tm.setTarget(4,somefunc,["variable X"])
tm.setTarget(5,somefunc,["more nonsense"])
print("-------------Starting with different functions")
tm.resumeAll()
print("in between")
sleep(1)
print("in between")
sleep(1)
print("in between")
sleep(1)
print("in between")
sleep(1)
print("in between")
sleep(1)
print("-------------Quitting")
tm.terminateAll()


