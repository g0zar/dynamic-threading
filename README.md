dynamic-threading
=================
(Imported from github: https://github.com/g0zar/dynamic-threading)
Addition for python3's threading.py module.
The purpose of this module is to allow for threads to be suspended and resumed without the need to spawn new threads every time a function
needs to be run more than once.

Features include:

- "Resume" thread after function has finished processing. Equivalent to starting the thread again.
- Terminate threads after you don't need them anymore. Possible only if threads finished their job.
- Change the function to be executed and arguments while the thread is suspended
- Check if threads have finished processing the given function
- A block() function which allows other threads to wait for a function to finish processing in another thread

##### examples.py file

```python
from dynamicthreading import SmartThread
from time import sleep

def onefunc(variable):
    sleep(1)
    print("Onefunc:"+str(variable))

def twofunc():
    sleep(1)
    print("No argument func")

def waitfunc():
    print("Waiting")
    sleep(5)
    print("Done")

def dbg(string):
    assert type(string) is str
    print("--------------------------------"+string)

def waitForThread(throbj):
    sleep(1)
    print("(Main thread)"+throbj.name+" finished?:"+str(throbj.is_finished()))
    throbj.block()
    print("(Main thread)"+throbj.name+" finished?:"+str(throbj.is_finished()))

def inthread_wait(throbj):
    print("(Reusable-2)Waiting for:"+throbj.name)
    throbj.block()
    print("(Reusable-2)Thread finished, exiting as well")

thread_list = []

thr1 = SmartThread(target=onefunc,args=("Some arg",),name="Reusable-1")
thr1.daemon = False
dbg("Starting first thread")
thr1.start()
waitForThread(thr1)
dbg("Resuming/Restaring first thread")
thr1.resume()
waitForThread(thr1)
dbg("Reassinging function and arguments for first thread")
thr1.setTarget(twofunc,[])
dbg("Restaring thread again")
thr1.resume()
waitForThread(thr1)
dbg("Reassinging function and arguments again")
thr1.setTarget(waitfunc,[])
thr1.resume()
waitForThread(thr1)

dbg("Starting inthread_wait test")
thr2 = SmartThread(target=inthread_wait,args=(thr1,),name="Reusable-2")
thr2.daemon = False
thr1.resume()
thr2.start()
waitForThread(thr1)
waitForThread(thr2)
thr1.terminate()
thr2.terminate()


```

##### Output from examples.py
```
--------------------------------Starting first thread
Onefunc:Some arg
(Main thread)Reusable-1 finished?:False
(Main thread)Reusable-1 finished?:True
--------------------------------Resuming/Restaring first thread
(Main thread)Reusable-1 finished?:False
Onefunc:Some arg
(Main thread)Reusable-1 finished?:True
--------------------------------Reassinging function and arguments for first thread
--------------------------------Restaring thread again
(Main thread)Reusable-1 finished?:False
No argument func
(Main thread)Reusable-1 finished?:True
--------------------------------Reassinging function and arguments again
Waiting
(Main thread)Reusable-1 finished?:False
Done
(Main thread)Reusable-1 finished?:True
--------------------------------Starting inthread_wait test
Waiting
(Reusable-2)Waiting for:Reusable-1
(Main thread)Reusable-1 finished?:False
Done
(Main thread)Reusable-1 finished?:True
(Reusable-2)Thread finished, exiting as well
(Main thread)Reusable-2 finished?:True
(Main thread)Reusable-2 finished?:True
```