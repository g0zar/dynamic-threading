from dynamicthreading import SmartThread
from time import sleep

def onefunc(variable):
    sleep(1)
    print("Onefunc:"+str(variable))

def twofunc():
    sleep(1)
    print("No argument func")

def waitfunc():
    print("Waiting")
    sleep(5)
    print("Done")

def dbg(string):
    assert type(string) is str
    print("--------------------------------"+string)

def waitForThread(throbj):
    sleep(1)
    print("(Main thread)"+throbj.name+" finished?:"+str(throbj.is_finished()))
    throbj.block()
    print("(Main thread)"+throbj.name+" finished?:"+str(throbj.is_finished()))

def inthread_wait(throbj):
    print("(Reusable-2)Waiting for:"+throbj.name)
    throbj.block()
    print("(Reusable-2)Thread finished, exiting as well")

thread_list = []

thr1 = SmartThread(target=onefunc,args=("Some arg",),name="Reusable-1")
thr1.daemon = False
dbg("Starting first thread")
thr1.start()
waitForThread(thr1)
dbg("Resuming/Restaring first thread")
thr1.resume()
waitForThread(thr1)
dbg("Reassinging function and arguments for first thread")
thr1.setTarget(twofunc,[])
dbg("Restaring thread again")
thr1.resume()
waitForThread(thr1)
dbg("Reassinging function and arguments again")
thr1.setTarget(waitfunc,[])
thr1.resume()
waitForThread(thr1)

dbg("Starting inthread_wait test")
thr2 = SmartThread(target=inthread_wait,args=(thr1,),name="Reusable-2")
thr2.daemon = False
thr1.resume()
thr2.start()
waitForThread(thr1)
waitForThread(thr2)
thr1.terminate()
thr2.terminate()

